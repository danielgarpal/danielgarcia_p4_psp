package sockets;


import javax.swing.*;

import java.awt.*;
import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

public class Servidor {

    public static void main(String[] args) {
        // TODO Auto-generated method stub

        MarcoServidor mimarco = new MarcoServidor();

        mimarco.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

    }
}

//Implementamos runnable
class MarcoServidor extends JFrame implements Runnable /*extends Thread*/{

    public MarcoServidor() {

        setBounds(1200, 300, 280, 350);

        JPanel milamina = new JPanel();

        milamina.setLayout(new BorderLayout());

        areatexto = new JTextArea();

        milamina.add(areatexto, BorderLayout.CENTER);

        add(milamina);

        setVisible(true);

        //Creamos el hilo
        Thread mihilo = new Thread(this);

        mihilo.start();

    }

    private JTextArea areatexto;

    @Override
    public void run() {

        //System.out.println("aki toi");
        //areatexto.setText("aki toi");

        try {
            ServerSocket servidor = new ServerSocket(5555);

            String nick;

            String ip;

           String mensaje;

           PaqueteEnvio paqueteRecibido;


            while(true) {

                Socket misocket = servidor.accept();

                //Creo flujo de datos de entrada
                ObjectInputStream paqueteDatosRecibidos = new ObjectInputStream(misocket.getInputStream());

                //Guardo el onbjeto recibido en la variable del paquete recibido
                paqueteRecibido = (PaqueteEnvio) paqueteDatosRecibidos.readObject();

                nick= paqueteRecibido.getNick();

                ip=paqueteRecibido.getIp();

                mensaje = paqueteRecibido.getMensaje();

                areatexto.append("\nIP DEST: " + ip + "\nNICK: " + nick + "\nMENSAJE: \n    " + mensaje );

                ////
                Socket enviaDestinatario = new Socket(ip,5050);

                ObjectOutputStream paqueteReenvio = new ObjectOutputStream(enviaDestinatario.getOutputStream());

                paqueteReenvio.writeObject(paqueteRecibido);

                enviaDestinatario.close();





                //Cerramos el socket
                misocket.close();

            }

        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }


    }
}

