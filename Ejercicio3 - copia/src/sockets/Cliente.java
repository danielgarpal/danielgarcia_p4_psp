package sockets;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;


public class Cliente {

    public static void main(String[] args) {
        // TODO Auto-generated method stub

        MarcoCliente mimarco = new MarcoCliente();

        mimarco.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

    }

}


class MarcoCliente extends JFrame {

    public MarcoCliente() {

        setBounds(600, 300, 280, 350);

        LaminaMarcoCliente milamina = new LaminaMarcoCliente();

        add(milamina);

        setVisible(true);
    }

}

class LaminaMarcoCliente extends JPanel implements Runnable {

    public LaminaMarcoCliente() {

        nick = new JTextField(5);

        add(nick);

        JLabel texto = new JLabel("SENCICHAT");

        add(texto);

        ipDestinatario = new JTextField(8);

        add(ipDestinatario);

        //Campo en el que se muestran los mensajes recibidos
        campochat = new JTextArea(12,20);

        campochat.setBackground(Color.lightGray);

        add(campochat);

        //Campo en el que se escribe el mensaje a enviar
        campo1 = new JTextField(20);

        add(campo1);

        miboton = new JButton("Enviar");

        EnviaTexto mievento = new EnviaTexto();

        miboton.addActionListener(mievento);

        add(miboton);

        Thread hilo = new Thread(this);

        hilo.start();



    }

    @Override
    public void run() {

        try {


            ServerSocket servidorCliente = new ServerSocket(5050);

            Socket cliente;

            PaqueteEnvio paqueteRecibido;

            while(true){

                cliente=servidorCliente.accept();

                ObjectInputStream flujoEntrada = new ObjectInputStream(cliente.getInputStream());

                paqueteRecibido= (PaqueteEnvio) flujoEntrada.readObject();

                campochat.append("\n " + paqueteRecibido.getNick() + " : " + paqueteRecibido.getMensaje());
            }


        } catch (IOException | ClassNotFoundException e) {
            System.out.println(e.getMessage());
        }

    }

    private class EnviaTexto implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent e) {

            //System.out.println(campo1.getText());

            try {
                //Creamos un socket
                Socket misocket = new Socket("localhost",5555);

                PaqueteEnvio datos = new PaqueteEnvio();



                //Pongo en la clase los datos recibidos en los JFlieds
                datos.setNick(nick.getText());

                datos.setIp(ipDestinatario.getText());

                datos.setMensaje(campo1.getText());


                //Flujo en la red
                ObjectOutputStream paqueteDatos = new ObjectOutputStream(misocket.getOutputStream());

                paqueteDatos.writeObject(datos);

                misocket.close();




            } catch (IOException ex) {
                //ex.printStackTrace();
                System.out.println(ex.getMessage());
            }


        }
    }


    private JTextField campo1;

    private JTextArea campochat;

    private JTextField nick;

    private JTextField ipDestinatario;

    private JButton miboton;

}



class PaqueteEnvio implements Serializable {

    private String nick;

    private String ip;

    private String mensaje;


    //GETTERS Y SETTERS
    public String getNick() {
        return nick;
    }

    public void setNick(String nick) {
        this.nick = nick;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }
}