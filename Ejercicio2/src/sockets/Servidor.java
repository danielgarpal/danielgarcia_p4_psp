package sockets;


import javax.swing.*;

import java.awt.*;
import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

/*
Clase servidor
 */
public class Servidor {

    /*
    Metodo main del Servidor, que crea y ejecuta la ventana del programa
     */
    public static void main(String[] args) {
        // TODO Auto-generated method stub

        //Creo un marco
        MarcoServidor mimarco = new MarcoServidor();

        //EL programa se termina al cerrar la ventana
        mimarco.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

    }
}

/*
Clase MarcoServidor, de tipo JFrame, que incorpora un hilo.
Crea el interior de la ventana sel Servidor y ejecuta el hilo
de entrada de socket.
 */
class MarcoServidor extends JFrame implements Runnable /*extends Thread*/{

    //Constructor de MarcoServidor
    public MarcoServidor() {

        //Tamaño y posicion de la ventana
        setBounds(1200, 300, 280, 350);

        //Creo un panel
        JPanel milamina = new JPanel();

        //Le pongo al panel el tipo de Layout
        milamina.setLayout(new BorderLayout());

        //creo un nuevo areatexto
        areatexto = new JTextArea();

        //Añado al marco el area de texto y lo centro
        milamina.add(areatexto, BorderLayout.CENTER);

        //Añado la lámina al marco
        add(milamina);

        //Hago visible el marco
        setVisible(true);

        //Creo el hilo
        Thread mihilo = new Thread(this);

        //Inicio el hilo
        mihilo.start();

    }

    //Creo un area de texto
    private JTextArea areatexto;

    //Metodo run del programa. Ejecuta el hilo que se encarga de leer constantemente
    //los mensajes enviados por el cliente
    @Override
    public void run() {

        //System.out.println("aki toi");
        //areatexto.setText("aki toi");

        try {
            //Creo un socket de servidor y le asigno un puerto
            ServerSocket servidor = new ServerSocket(5555);

            String nick;

            String ip;

           String mensaje;

           PaqueteEnvio paqueteRecibido;


            while(true) {

                //Creo un socket, acepto la conexion entrante y guardo su informacion en el socket
                Socket misocket = servidor.accept();

                //Creo el flujo de entrada de tipo DataInputStream
                ObjectInputStream paqueteDatosRecibidos = new ObjectInputStream(misocket.getInputStream());

                //Guardo el onbjeto recibido en la variable del paquete recibido
                paqueteRecibido = (PaqueteEnvio) paqueteDatosRecibidos.readObject();

                //Divido los datos del paquete y los guardo en variables
                nick= paqueteRecibido.getNick();

                ip=paqueteRecibido.getIp();

                mensaje = paqueteRecibido.getMensaje();

                //Añado las variables al campo de texto
                areatexto.append("\nIP DEST: " + ip + "\nNICK: " + nick + "\nMENSAJE: \n    " + mensaje );

                //Cerramos el socket
                misocket.close();

            }

        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }


    }
}

