package sockets;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

/*
Clase cliente que ejecuta la ventana del programa
 */
public class Cliente {

    public static void main(String[] args) {
        // TODO Auto-generated method stub

        MarcoCliente mimarco = new MarcoCliente();

        mimarco.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

    }

}


/*
Clase marco del cliente, que extiende de Frame.
Establece las medidas de la ventana y crea un panel,
despues lo introduce al marco y lo hace visible
 */
class MarcoCliente extends JFrame {

    //Constructor de la clase MarcoCLiente
    public MarcoCliente() {

        //Tamaño y posicion de la ventana
        setBounds(600, 300, 280, 350);

        //Se crea una lamina milamina
        LaminaMarcoCliente milamina = new LaminaMarcoCliente();

        //Se añade la lamina al marco
        add(milamina);

        //Hago visible el marco con la lamina dentro
        setVisible(true);
    }

}

/*
Clase lamina del cliente, que extiende de Panel.
Crea un campo para el nick, un texto para el nombre del chat, un campo
para la ip del destinatario, un campo para escribir y un boton para enviar. Al botón
le asigna un listener que "escuchará" cuando este sea pulsado.
 */
class LaminaMarcoCliente extends JPanel implements Runnable {

    //Constructor de la clase LaminaMarcoCliente
    public LaminaMarcoCliente() {

        //Creo un campo para el nick
        nick = new JTextField(5);

        //Añado el nick al panel
        add(nick);

        //Creo una etiqueta para el nombre del caht
        JLabel texto = new JLabel("SENCICHAT");

        //Añado el campo al marco
        add(texto);

        //Creo un campo para la ip del destinatario
        ipDestinatario = new JTextField(8);

        //Añado el campo al marco
        add(ipDestinatario);


        //Campo en el que se muestran los mensajes recibidos
        campochat = new JTextArea(12,20);

        //Cambio el color de fondo
        campochat.setBackground(Color.lightGray);

        //Añado el campo de chat a la ventana
        add(campochat);


        //Campo en el que se escribe el mensaje a enviar
        campo1 = new JTextField(20);

        add(campo1);

        //Creo un boton para enviar
        miboton = new JButton("Enviar");

        //Creo un evento de la clase EnviarTexto que servirá como listener para el boton
        EnviaTexto mievento = new EnviaTexto();

        //Añado el listener al boton
        miboton.addActionListener(mievento);

        //Añado el boton a la ventana
        add(miboton);

        //Creo un hilo
        Thread hilo = new Thread(this);

        //Inicio el hilo
        hilo.start();



    }

    /*
    Hilo que lee constantemente mensajes que recibe del servidor
     */
    @Override
    public void run() {

        try {

            //Creo un Socket de tipo servidor
            ServerSocket servidorCliente = new ServerSocket(5050);

            //Creo un socket
            Socket cliente;

            //Creo un paquete para guardar los datos recibidos
            PaqueteEnvio paqueteRecibido;

            while(true){

                //Acepto la conexion entrante
                cliente=servidorCliente.accept();

                //Abro el flujo de entrada
                ObjectInputStream flujoEntrada = new ObjectInputStream(cliente.getInputStream());

                //Guardo el objeto recibido
                paqueteRecibido= (PaqueteEnvio) flujoEntrada.readObject();

                //Añado los datos recibidos al campo de texto
                campochat.append("\n " + paqueteRecibido.getNick() + " : " + paqueteRecibido.getMensaje());
            }


        } catch (IOException | ClassNotFoundException e) {
            System.out.println(e.getMessage());
        }

    }
    /*
        Clase EnviaTexto, que implementa ActionListener.
        Cuando se pulsa el botón enviar esta clase crea un socket
        y un paquete de datos que mas tarde envía, conteniendo la informacion.
        */
    private class EnviaTexto implements ActionListener{

        //Metodo que se ejecuta al ser activado el listener y contiene la acción que se va a llevar a cabo
        @Override
        public void actionPerformed(ActionEvent e) {

            //Añado al campo de texto el mensaje escrito
            campochat.append("\nYo: " + campo1.getText());





            try {
                //Creamos un socket
                Socket misocket = new Socket("localhost",5555);

                //Creo una clase  paquete para guardar los datos
                PaqueteEnvio datos = new PaqueteEnvio();



                //Pongo en la clase los datos recibidos en los JFlieds
                datos.setNick(nick.getText());

                datos.setIp(ipDestinatario.getText());

                datos.setMensaje(campo1.getText());


                //Flujo en la red
                ObjectOutputStream paqueteDatos = new ObjectOutputStream(misocket.getOutputStream());

                //Escribo el objeto de los datos en en outputStream
                paqueteDatos.writeObject(datos);

                //Cierro el socket
                misocket.close();




            } catch (IOException ex) {
                //ex.printStackTrace();
                System.out.println(ex.getMessage());
            }


        }
    }


    private JTextField campo1;

    private JTextArea campochat;

    private JTextField nick;

    private JTextField ipDestinatario;

    private JButton miboton;

}



/*
Clase PaqueteEnvio, que implementa serializable.
Guarda los datos que va a enviar en campos y los
serializa para poder mandarlos a traves del socket
 */class PaqueteEnvio implements Serializable {

    private String nick;

    private String ip;

    private String mensaje;


    //GETTERS Y SETTERS
    public String getNick() {
        return nick;
    }

    public void setNick(String nick) {
        this.nick = nick;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }
}