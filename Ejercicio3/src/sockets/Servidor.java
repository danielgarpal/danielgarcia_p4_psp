package sockets;


import javax.swing.*;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

/*
Clase servidor
 */
public class Servidor {

    /*
    Metodo main del Servidor, que crea y ejecuta la ventana del programa
     */
    public static void main(String[] args) {
        // TODO Auto-generated method stub

        //Creo un marco
        LaminaMarcoServidor mimarco = new LaminaMarcoServidor();

        //EL programa se termina al cerrar la ventana
        mimarco.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

    }
}

//Implementamos runnable
class LaminaMarcoServidor extends JFrame implements Runnable /*extends Thread*/{

    //Constructor de la lamina
    public LaminaMarcoServidor() {

        //Tamaño y posicion de la ventana
        setBounds(1200, 300, 280, 350);

        //Creo un panel
        JPanel milamina = new JPanel();


        //creo un nuevo areatexto
        areatexto = new JTextArea(12,20);

        //Añado al marco el area de texto
        milamina.add(areatexto);



        //Creo un campo para escribir el mensaje
        campo1 = new JTextField(20);

        milamina.add(campo1);

        //Creo un boton para enviar
        miboton = new JButton("Enviar");

        //Creo un evento de la clase EnviarTexto que servirá como listener para el boton
        EnviaTexto mievento = new EnviaTexto();

        //Añado el listener al boton
        miboton.addActionListener(mievento);

        milamina.add(miboton);


        //Añado la lamina
        add(milamina);

        setVisible(true);

        //Creamos el hilo
        Thread mihilo = new Thread(this);

        mihilo.start();

    }

    private JTextArea areatexto;
    private JTextField campo1;
    private JButton miboton;

    //Metodo run del programa. Ejecuta el hilo que se encarga de leer constantemente
    //los mensajes enviados por el cliente
    @Override
    public void run() {


        try {
            //Creo un socket de servidor y le asigno un puerto
            ServerSocket servidor = new ServerSocket(5555);

            String nick;

            String ip;

            String mensaje;

            PaqueteEnvio paqueteRecibido;


            while(true) {

                //Creo un socket, acepto la conexion entrante y guardo su informacion en el socket
                Socket misocket = servidor.accept();

                //Creo flujo de datos de entrada
                ObjectInputStream paqueteDatosRecibidos = new ObjectInputStream(misocket.getInputStream());

                //Guardo el onbjeto recibido en la variable del paquete recibido
                paqueteRecibido = (PaqueteEnvio) paqueteDatosRecibidos.readObject();

                //Divido los datos del paquete y los guardo en variables
                nick= paqueteRecibido.getNick();

                ip=paqueteRecibido.getIp();

                mensaje = paqueteRecibido.getMensaje();

                //Añado las variables al campo de texto
                areatexto.append("\nIP DEST: " + ip + "\nNICK: " + nick + "\nMENSAJE: \n    " + mensaje );






                //Cerramos el socket
                misocket.close();

            }

        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }


    }
    /*
    Clase EnviaTexto, que implementa ActionListener.
    Cuando se pulsa el botón enviar esta clase crea un socket
    y un paquete de datos que mas tarde envía, conteniendo la informacion.
    */
    private class EnviaTexto implements ActionListener {

        //Metodo que se ejecuta al ser activado el listener y contiene la acción que se va a llevar a cabo
        @Override
        public void actionPerformed(ActionEvent e) {

            //Añado al cuadro de texto el mensaje escrito
            areatexto.append("\nYo: " + campo1.getText());





            try {
                //Creamos un socket
                Socket misocket = new Socket("localhost",5050);

                //Creo una clase  paquete para guardar los datos
                PaqueteEnvio datos = new PaqueteEnvio();



                //Pongo en la clase los datos recibidos en los JFlieds

                datos.setMensaje(campo1.getText());
                datos.setNick("Servidor");


                //Flujo en la red
                ObjectOutputStream paqueteDatos = new ObjectOutputStream(misocket.getOutputStream());

                //Escribo el objeto de los datos en en outputStream
                paqueteDatos.writeObject(datos);

                //Cierro el socket
                misocket.close();




            } catch (IOException ex) {
                //ex.printStackTrace();
                System.out.println(ex.getMessage());
            }


        }
    }

}