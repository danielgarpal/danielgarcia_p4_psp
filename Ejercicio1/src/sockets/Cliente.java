package sockets;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;


/*
Clase cliente que ejecuta la ventana del programa
 */
public class Cliente {

    public static void main(String[] args) {
        // TODO Auto-generated method stub

        MarcoCliente mimarco = new MarcoCliente();

        mimarco.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

    }

}


/*
Clase marco del cliente, que extiende de Frame.
Establece las medidas de la ventana y crea un panel,
despues lo introduce al marco y lo hace visible
 */
class MarcoCliente extends JFrame {

    //Constructor de la clase MarcoCLiente
    public MarcoCliente() {

        //Tamaño y posicion de la ventana
        setBounds(600, 300, 280, 350);

        //Se crea una lamina milamina
        LaminaMarcoCliente milamina = new LaminaMarcoCliente();

        //Se añade la lamina al marco
        add(milamina);

        //Hago visible el marco con la lamina dentro
        setVisible(true);
    }

}

/*
Clase lamina del cliente, que extiende de Panel.
Crea una etiqueta, un campo para escribir y un boton. Al botón
le asigna un listener que "escuchará" cuando este sea pulsado.
 */
class LaminaMarcoCliente extends JPanel {

    //Constructor de la clase LaminaMarcoCliente
    public LaminaMarcoCliente() {

        //Creo una etiqueta "CLIENTE"
        JLabel texto = new JLabel("CLIENTE");

        //Añado la etiqueta al panel
        add(texto);

        //Creo un campo para escribir del tipo TextField
        campo1 = new JTextField(20);

        //Añado el campo al marco
        add(campo1);

        //Creo un boton para enviar
        miboton = new JButton("Enviar");

        //Creo un evento de la clase EnviarTexto que servirá como listener para el boton
        EnviaTexto mievento = new EnviaTexto();

        //Añado el listener al boton
        miboton.addActionListener(mievento);

        //Añado el boton al panel
        add(miboton);



    }

    /*
    Clase EnviaTexto, que implementa ActionListener.
    Cuando se pulsa el botón enviar esta clase crea un socket
    y envía por el el mensaje introducido en el campo de escritura
     */
    private class EnviaTexto implements ActionListener{

        //Metodo que se ejecuta al ser activado el listener y contiene la acción que se va a llevar a cabo
        @Override
        public void actionPerformed(ActionEvent e) {



            try {
                //Creo un socket
                Socket misocket = new Socket("localhost",5555);
                //Inicio la salida de datos
                DataOutputStream flujoSalida = new DataOutputStream(misocket.getOutputStream());
                //Escribo en el inputstream lo introducido en el campo de texto
                flujoSalida.writeUTF(campo1.getText());
                //Cierro el flujo
                flujoSalida.close();




            } catch (IOException ex) {
                //Imprimo el error en el caso de que suceda
                System.out.println(ex.getMessage());
            }


        }
    }


    private JTextField campo1;

    private JButton miboton;

}