package sockets;


import javax.swing.*;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

public class Servidor {

    public static void main(String[] args) {
        // TODO Auto-generated method stub

        LaminaMarcoServidor mimarco = new LaminaMarcoServidor();

        mimarco.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

    }
}

//Implementamos runnable
class LaminaMarcoServidor extends JFrame implements Runnable /*extends Thread*/ {

    public LaminaMarcoServidor() {

        setBounds(1200, 300, 280, 350);

        JPanel milamina = new JPanel();

        //milamina.setLayout(new BorderLayout());

        areatexto = new JTextArea(12, 20);

        milamina.add(areatexto);


        campo1 = new JTextField(20);

        milamina.add(campo1);

        miboton = new JButton("Enviar");

        EnviaTexto mievento = new EnviaTexto();

        miboton.addActionListener(mievento);

        milamina.add(miboton);


        add(milamina);

        setVisible(true);

        //Creamos el hilo
        Thread mihilo = new Thread(this);

        mihilo.start();

    }

    private JTextArea areatexto;
    private JTextField campo1;
    private JButton miboton;

    @Override
    public void run() {

        //System.out.println("aki toi");
        //areatexto.setText("aki toi");

        try {
            ServerSocket servidor = new ServerSocket(5555);

            String nick;

            String ip;

            String mensaje;

            PaqueteEnvio paqueteRecibido;


            while (true) {

                Socket misocket = servidor.accept();

                //Creo flujo de datos de entrada
                ObjectInputStream paqueteDatosRecibidos = new ObjectInputStream(misocket.getInputStream());

                //Guardo el onbjeto recibido en la variable del paquete recibido
                paqueteRecibido = (PaqueteEnvio) paqueteDatosRecibidos.readObject();

                nick = paqueteRecibido.getNick();

                ip = paqueteRecibido.getIp();

                mensaje = paqueteRecibido.getMensaje();

                areatexto.append("\nIP DEST: " + ip + "\nNICK: " + nick + "\nMENSAJE: \n    " + mensaje);


                //Cerramos el socket
                misocket.close();

            }

        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }


    }

    private class EnviaTexto implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {


            areatexto.append("\nYo: " + campo1.getText());


            //System.out.println(campo1.getText());

            try {
                //Creamos un socket
                Socket misocket = new Socket("localhost", 5050);

                PaqueteEnvio datos = new PaqueteEnvio();


                //Pongo en la clase los datos recibidos en los JFlieds

                datos.setMensaje(campo1.getText());
                datos.setNick("Servidor");


                //Flujo en la red
                ObjectOutputStream paqueteDatos = new ObjectOutputStream(misocket.getOutputStream());

                paqueteDatos.writeObject(datos);

                misocket.close();


            } catch (IOException ex) {
                //ex.printStackTrace();
                System.out.println(ex.getMessage());
            }


        }
    }

}